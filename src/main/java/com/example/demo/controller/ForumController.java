package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

//追加
@Controller
public class ForumController {

	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表示画面・返信内容表示画面
	@GetMapping
	public ModelAndView top() {
		ModelAndView mav = new ModelAndView();
		// 投稿を全件取得
		List<Report> contentData = reportService.findAllReport();
		//返信を全件取得
		List<Comment> commentData = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		// 返信データを保管
		mav.addObject("comments", commentData);
		return mav;
	}

	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	@GetMapping("/newComment/{id}")
	public ModelAndView newComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();

		Comment comment = new Comment();

		mav.setViewName("/newComment");

		comment.setContentId(id);

		mav.addObject("commentModel", comment);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {

		reportService.deleteReport(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 編集画面遷移
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();

		Report report = reportService.editReport(id);

		mav.setViewName("/edit");

		mav.addObject("formModel", report);
		return mav;
	}

	// 編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {

		report.setId(id);

		reportService.saveReport(report);

		return new ModelAndView("redirect:/");
	}
}
