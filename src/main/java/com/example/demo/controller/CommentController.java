package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.service.CommentService;

@Controller
public class CommentController {
	@Autowired
	CommentService commentService;
	// コメント投稿処理
	@PutMapping("/addComment/{id}")
	public ModelAndView addComment(@ModelAttribute("commentModel") Comment comment,@PathVariable Integer id) {
	// 投稿をテーブルに格納
	// comment.setContentId(id);
	commentService.saveComment(comment);
	// rootへリダイレクト
	return new ModelAndView("redirect:/");
	}
}
